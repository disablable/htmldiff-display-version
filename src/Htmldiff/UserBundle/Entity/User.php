<?php

namespace Htmldiff\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;

use Htmldiff\PageBundle\Entity\PageGroup;
use Htmldiff\PageBundle\Entity\Tag;

/**
 * User
 *
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="Htmldiff\UserBundle\Entity\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Htmldiff\PageBundle\Entity\PageGroup", mappedBy="user")
     */
    private $pageGroups;

    /**
     * @ORM\OneToMany(targetEntity="Htmldiff\PageBundle\Entity\Tag", mappedBy="user")
     */
    private $tags;


    public function __construct()
    {
        parent::__construct();
        // your own logic here..

        $this->pageGroups = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function addPageGroup(PageGroup $pageGroup)
    {
        $pageGroup->addUser($this); // synchronously updating inverse side
        $this->pageGroups[] = $pageGroup;

        return $this;
    }

    public function getPageGroups()
    {
        return $this->pageGroups;
    }

    public function addTag(Tag $tag)
    {
        $tag->addUser($this); // synchronously updating inverse side
        $this->tags[] = $tag;

        return $this;
    }

    public function getTags()
    {
        return $this->tags;
    }
}

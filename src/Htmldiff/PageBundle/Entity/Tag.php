<?php

namespace Htmldiff\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Htmldiff\UserBundle\Entity\User;
use Htmldiff\PageBundle\Entity\Page;

/**
 * Tag
 *
 * @ORM\Table(name="tag")
 * @ORM\Entity(repositoryClass="Htmldiff\PageBundle\Entity\TagRepository")
 */
class Tag
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Htmldiff\UserBundle\Entity\User", inversedBy="tags")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     **/
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="Htmldiff\PageBundle\Entity\Page", mappedBy="tags")
     */
    private $pages;

    /**
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @ORM\Column(name="color", type="string")
     */
    private $color;


    public function __construct()
    {
        $this->pages = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    // This function is automatycally called when Page::addTag() is called, so use that instead
    // (Entity "Page" is owning side of this relation)
    public function addPage(Page $page)
    {
        $this->pages[] = $page;

        return $this;
    }

    public function getPages()
    {
        return $this->pages;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    public function getColor()
    {
        return $this->color;
    }
}

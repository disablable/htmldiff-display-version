<?php

namespace Htmldiff\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Htmldiff\PageBundle\Entity\PageGroup;

/**
 * Page
 *
 * @ORM\Table(name="page")
 * @ORM\Entity(repositoryClass="Htmldiff\PageBundle\Entity\PageRepository")
 */
class Page
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Htmldiff\PageBundle\Entity\PageGroup", inversedBy="pages")
     * @ORM\JoinColumn(name="page_group_id", referencedColumnName="id")
     **/
    private $pageGroup;

    /**
     * @ORM\ManyToMany(targetEntity="Htmldiff\PageBundle\Entity\Tag", inversedBy="pages")
     * @ORM\JoinTable(name="pages_tags")
     **/
    private $tags;

    /**
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @ORM\Column(name="url", type="string")
     */
    private $url;

    /**
     * @ORM\Column(name="archive_interval_expr", type="string")
     */
    private $archiveIntervalExpr;

    /**
     * @ORM\Column(name="archive_interval_unit", type="string")
     */
    private $archiveIntervalUnit;

    /**
     * @ORM\Column(name="archive_updated_on", type="datetime", nullable=TRUE)
     */
    private $archiveUpdatedOn;

    /**
     * @ORM\Column(name="archive_update_status", type="string", nullable=TRUE)
     */
    private $archiveUpdateStatus;


    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

// temp for testing
public function setId($id)
{
    $this->id = $id;
}

    public function getId()
    {
        return $this->id;
    }

    public function setPageGroup(PageGroup $pageGroup)
    {
        $this->pageGroup = $pageGroup;

        return $this;
    }

    public function getPageGroup()
    {
        return $this->pageGroup;
    }

    public function addTag(Tag $tag)
    {
        $tag->addPage($this); // synchronously updating inverse side
        $this->tags[] = $tag;

        return $this;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setArchiveIntervalExpr($archiveIntervalExpr)
    {
        $this->archiveIntervalExpr = $archiveIntervalExpr;

        return $this;
    }

    public function getArchiveIntervalExpr()
    {
        return $this->archiveIntervalExpr;
    }

    public function setArchiveIntervalUnit($archiveIntervalUnit)
    {
        $this->archiveIntervalUnit = $archiveIntervalUnit;

        return $this;
    }

    public function getArchiveIntervalUnit()
    {
        return $this->archiveIntervalUnit;
    }

    public function setArchiveUpdatedOn($archiveUpdatedOn)
    {
        $this->archiveUpdatedOn = $archiveUpdatedOn;

        return $this;
    }

    public function getArchiveUpdatedOn()
    {
        return $this->archiveUpdatedOn;
    }

    public function setArchiveUpdateStatus($archiveUpdateStatus)
    {
        $this->archiveUpdateStatus = $archiveUpdateStatus;

        return $this;
    }

    public function getArchiveUpdateStatus()
    {
        return $this->archiveUpdateStatus;
    }
}

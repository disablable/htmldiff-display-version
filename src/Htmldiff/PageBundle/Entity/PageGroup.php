<?php

namespace Htmldiff\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Htmldiff\UserBundle\Entity\User;
use Htmldiff\PageBundle\Entity\Page;

/**
 * PageGroup
 *
 * @ORM\Table(name="page_group")
 * @ORM\Entity(repositoryClass="Htmldiff\PageBundle\Entity\PageGroupRepository")
 */
class PageGroup
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Htmldiff\UserBundle\Entity\User", inversedBy="pageGroups")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     **/
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="Htmldiff\PageBundle\Entity\Page", mappedBy="pageGroup")
     */
    private $pages;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    public function __construct()
    {
        $this->pages = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function addPage(Page $page)
    {
        $this->pages[] = $page;

        return $this;
    }

    public function getPages()
    {
        return $this->pages;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }
}

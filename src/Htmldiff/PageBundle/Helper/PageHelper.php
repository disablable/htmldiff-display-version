<?php

namespace Htmldiff\PageBundle\Helper;

class PageHelper
{
    public static function getIntervalOptions()
    {
        return [
            '1h'  => '1 hour',
            '12h' => '12 hours',
            '1d'  => '1 day',
            '4d'  => '4 days',
            '1w'  => '1 week',
            '1m'  => '1 month',
        ];
    }

    public static function getDateIntervalStringFromIntervalOption($interval)
    {
        $intervals = [
            '1h'  => 'PT1H',
            '12h' => 'PT12H',
            '1d'  => 'P1D',
            '4d'  => 'P4D',
            '1w'  => 'P1W',
            '1m'  => 'P1M',
        ];

        return $intervals[$interval];
    }
}

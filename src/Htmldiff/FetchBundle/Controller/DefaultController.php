<?php

namespace Htmldiff\FetchBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/fetch-url", name="fetch_url")
     */
    public function fetchUrlAction()
    {
        $fetchService = $this->get('htmldiff.fetch');
        $fetchService->fetchUrl('http://www.lifehacker.com');

        return [];
    }
}

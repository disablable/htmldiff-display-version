<?php

namespace Htmldiff\ArchiveBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * PageArchiveUpdateCommand
 * This worker command updates page archive by given page_id
 */
class PageArchiveUpdateCommand extends ContainerAwareCommand
{
    private $logger;
    private $em;

    protected function configure()
    {
        $this
            ->setName('htmldiff:archive:page:update')
            ->setDescription('Command for updating page in archive by given page id')
            ->addArgument(
                'archive_id',
                InputArgument::REQUIRED,
                'Id of archive'
            )
            ->addArgument(
                'page_id',
                InputArgument::REQUIRED,
                'Id of page you want to update'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = $this->getContainer()->get('logger');
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $archiveId = (int)$input->getArgument('archive_id');
        $pageId = (int)$input->getArgument('page_id');

        $logger->info("Updating page id: {$pageId}");

        $archivePageService = $this->getContainer()->get('htmldiff.archive.page');

        $archiveRepo = $em->getRepository('HtmldiffArchiveBundle:Archive');
        $archive = $archiveRepo->findOneById($archiveId);

        if (!$archive) {
            $logger->error("Unable to find archive by id: {$archiveId}");
            exit();
        }

        $pageRepo = $em->getRepository('HtmldiffPageBundle:Page');
        $page = $pageRepo->findOneById($pageId);

        if (!$page) {
            $logger->error("Unable to find page by id: {$pageId}");
            exit();
        }

        try {
            $archivePageService->updatePage($archive, $page);

            $page->setArchiveUpdatedOn(new \DateTime());
            $page->setArchiveUpdateStatus('OK');

            $em->persist($page);
            $em->flush();
        } catch (\Exception $e) {
            // Catching exception for saving error message to page
            $page->setArchiveUpdatedOn(new \DateTime());
            $page->setArchiveUpdateStatus('Error: ' . $e->getMessage());
            $em->persist($page);
            $em->flush();

            throw new \Exception($e);
        }

        $logger->info("Finished updating page id: {$pageId}");
    }
}

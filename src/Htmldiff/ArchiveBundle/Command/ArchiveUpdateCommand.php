<?php

namespace Htmldiff\ArchiveBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * ArchiveUpdateCommand
 * This command updates all archive by spawning workers to update pages one by one
 */
class ArchiveUpdateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('htmldiff:archive:update')
            ->setDescription('Updates whole archive by spawning workers to update pages one by one.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = $this->getContainer()->get('logger');
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $logger->info("Started archive update");

        // Get all pages scheduled for update
        $pageRepo = $em->getRepository('HtmldiffPageBundle:Page');
        $pageIds = $pageRepo->findPageIdsScheduledForUpdate();

        if (empty($pageIds)) {
            $logger->info("No pages scheduled for update");
            exit();
        }

        $logger->info("Creating new archive");

        // Create new archive
        $archive = new Archive();
        $archive->setPageCount(count($pageIds));
        $archive->setStartedOn(new \DateTime());
        $em->persist($archive);
        $em->flush();

        // Add pages to page-update gearman queue
        $gmc = new \GearmanClient();
        $gmc->addServer();

        $jobHandles = [];
        foreach ($pageIds as $pageId) {
            $logger->info("Adding page to queue: " . $pageId);
            $jobHandles[] = $gmc->doBackground('htmldiff_archive_update_page', serialize(['archive_id' => $archive->getId(), 'page_id' => $pageId]));
        }

        $logger->info("Waiting for htmldiff_archive_update_page workers to complete..");

        // Wait for all jobs to finish by checking if their job handle exists
        do {
            sleep(2);
            $done = true;
            foreach ($jobHandles as $jobHandle) {
                $status = $gmc->jobStatus($jobHandle);
                // The first status array element is a boolean indicating whether the job is even known
                if ($status[0]) {
                    // The job is known, so it is not finished jet
                    $done = false;
                }
            }
        } while (!$done);

        // Update finished time for Archive
        $archive->setFinishedOn(new \DateTime());
        $em->persist($archive);
        $em->flush();

        $logger->info("Finished archive update");
    }
}

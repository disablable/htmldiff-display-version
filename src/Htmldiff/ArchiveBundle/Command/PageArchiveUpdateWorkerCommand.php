<?php

namespace Htmldiff\ArchiveBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * ArchiveUpdatePageWorkerCommand
 * This worker command updates page by given page_id
 */
class PageArchiveUpdateWorkerCommand extends ContainerAwareCommand
{
    private $logger;
    private $em;

    protected function configure()
    {
        $this
            ->setName('htmldiff:archive:page:update:worker')
            ->setDescription('Gearman worker for updating page in archive by given page id')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->logger = $this->getContainer()->get('logger');
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');

        // Register command as gearman worker
        $gmw = new \GearmanWorker();
        $gmw->addServers();
        $gmw->setTimeout(600000);  // Wait 10 minutes for a job, then exit (supervisor will restart process)
        $gmw->addFunction('htmldiff_archive_update_page', [$this, 'updatePage']);

        $this->logger->info("Worker up! Waiting for job..");

        while ($gmw->work()) {
            $this->logger->info("Worker return code: " . $gmw->returnCode());
        }

        $this->logger->info("Worker exiting");
    }

    public function updatePage(\GearmanJob $job)
    {
        $workload = unserialize($job->workload());
        $archiveId = $workload['archive_id'];
        $pageId = $workload['page_id'];

        $pageUpdateService = $this->getContainer()->get('htmldiff.page.update');

        $pageUpdate = $pageUpdateService->createPageUpdate($archiveId, $pageId);









        try {
            $archivePageService = $this->getContainer()->get('htmldiff.archive.page');
            $archivePageService->createPageUpdate($pageUpdate);

            //$page->setArchiveUpdatedOn(new \DateTime());  // temp disabled
            $page->setArchiveUpdateStatus('OK');

            $this->em->persist($page);
            $this->em->flush();
        } catch (\Exception $e) {
            // Catching exception for saving error message to page
            //$page->setArchiveUpdatedOn(new \DateTime());  // temp disabled
            $page->setArchiveUpdateStatus('Error: ' . $e->getMessage());
            $this->em->persist($page);
            $this->em->flush();

            // Rethrow exception so job is removed from gearman queue ??
            throw new \Exception($e);
        }

        $this->logger->info("Finished updating page: {$pageId}");
    }
}

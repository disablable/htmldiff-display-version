<?php

namespace Htmldiff\ArchiveBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Htmldiff\ArchiveBundle\Entity\Archive;
use Htmldiff\PageBundle\Entity\Page;
use Htmldiff\ArchiveBundle\Entity\PageArchive;

/**
 * ArchiveUpdateCommand
 * This command updates all archive by spawning workers to update pages one by one
 */
class TestCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('htmldiff:test');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $cssContent = file_get_contents('/var/www/htmldiff/storage/archive/782/1442442895/assets/a58f019643e6fbe08e3d3faa0202ec07');
        $cssContent = preg_replace_callback(
            '/(?<=url\()[^\)]+(?=\))/',    // match URL wrapped inside "url()" string but without that wrapper
            function ($matches) {
                var_dump($matches);
            },
            $cssContent
        );









        exit;
        // $cmd = 'curl -o storage/archive/assets/ubuntu.iso "http://releases.ubuntu.com/14.04.3/ubuntu-14.04.3-desktop-amd64.iso"';
        // $outputfile = '/var/www/htmldiff/storage/archive/assets/output.txt';
        // $pidArr = [];
        // //exec(sprintf("%s > %s 2>&1 & echo $! >> %s", $cmd, $outputfile, $pidfile));

        // exec(sprintf("%s > %s 2>&1 & echo $!", $cmd, $outputfile), $pidArr);

        // var_dump($pidArr);

// $src = '../Img/separator/vertical-divider-300-d6d6d6.png';
// $url = 'http://static.tportal.hr/WebResources/tportal201009/Img/separator/vertical-divider-300-d6d6d6.png';

// $str3 = 'http://adsvc.tportal.hr/www/delivery/lg.php?bannerid=14897&campaignid=3866&zoneid=272&loc=http%3A%2F%2Fwww.tportal.hr%2F&cb=49540f4ecf#kurac';

// If URL is relative, remove ".." so it can be found in absoute URL path
// if (substr($src, 0, 2) == '..') {
//     $src = substr_replace($src, '', 0, 2);
// }

// var_dump($src);
// var_dump(strpos($url, $src));


// print_r($path = parse_url($str3));
//print_r($path = parse_url($str2, ));

//         $cssContent = 'padding: 0 15px 0 45px;
//     background: #494949 url(http://static.tportal.hr/webres/images/sso111.png) no-repeat 0 -30px;
// }

// #ssotb li ul li a:hover {
//     color: #eee;
//     text-decoration: none;
//     background: #2c2c2c url(http://static.tportal.hr/webres/images/sso2222.png) no-repeat 0 0;
// }
// ';
//         $arr = [];
//         // Match URL inside "url()" markup, but without "url()" (e.g. "url(www.site.com)" returns "www.site.com")
//         $matches = [];
//         preg_match('/(?<=url\()[^)]+/', $cssContent, $matches);
//         //print_r($matches);

//         var_dump(preg_replace_callback(
//             '/(?<=url\()[^)]+/',
//             function ($matches) use ($arr) {
//                 // If URL is relative, remove ".." so it can be found in absoute URL path
//                 // if (substr($src, 0, 2) == '..') {
//                 //     $src = substr_replace($src, '', 0, 2);
//                 // }
//                 echo("\n======\n");
//                 print_r($matches[0]);
//                 echo("\n======\n");
//                 return 'www.kurac.com';
//             },
//             $cssContent
//         ));


//         exit("stop");

        $fetchService = $this->getContainer()->get('htmldiff.fetch');
        $fileStorageService = $this->getContainer()->get('htmldiff.fileStorage');
        $diffService =  $this->getContainer()->get('htmldiff.diff');

        // Create new archive
        $archive = new Archive();
        $archive->setPageCount(1);
        $archive->setStartedOn(new \DateTime());

        $page = new Page();
        $page->setUrl('http://www.lifehacker.com');
        //$page->setUrl('http://www.tportal.hr');
        $page->setId(782);

        // Create new PageArchive entity
        $pageArchive = new PageArchive();
        $pageArchive->setArchive($archive);
        $pageArchive->setPage($page);

        $fetchService->fetchHtml($pageArchive);

        // Prepare fetched page source html for diff process
        //$processedPageHtml = $diffService->prepareHtmlForDiff($pageArchive->getHtml());
        //$pageArchive->setHtml($processedPageHtml);


// !!! Mozda prvo napraviti listu asseta koji u sebi imaju podatke iz HARa + napravljene download pathove sa imenima fajlova.
// A onda nakon sto pretrcim kroz HTML i CSS-ove i nadjem gdje cu "replace"ati URLove sa putanjama do fajlova
// tada cu znati i koje requestova iz HARA zapravo koristim i tek tada samo njih downloadam. Na taj nacin necu downlodati assete
// koji se niti ne koriste !!!


        $fetchService->downloadAssets($pageArchive);

        //$newPageArchiveFile = $fileStorageService->addPageArchiveFile($pageArchive);


        //
        // Find elements with src attributes in HAR array and replace their paths with according downloaded asset file paths
        // Find link elements in HAR array and replace their paths with according downloaded asset file paths
        //
        // TODO: When fixing relative URLs handle multiple ".." (e.g. "../../../some.css")
        //

        $doc = new \DOMDocument();
        $doc->loadHTML($pageArchive->getHtml());
        $xPath = new \DOMXPath($doc);

        $nodes = $xPath->query('//*[@src] | //link[@href]');
        $assets = $pageArchive->getAssets();
        foreach ($nodes as $node) {
            if (!empty($node->getAttribute('src'))) {
                $url = $node->getAttribute('src');
                $nodeType = 'src';
            } else {
                $url = $node->getAttribute('href');
                $nodeType = 'href';
            }

            // If URL is relative, remove ".." so it can be found in absoute URL path
            if (substr($url, 0, 2) == '..') {
                $url = substr_replace($url, '', 0, 2);
            }

            foreach ($assets as $asset) {
                $assetUrl = $asset->getUrl();
                if (strpos($asset->getUrl(), $url) !== false) {
                    if ($nodeType == 'src') {
                        $node->setAttribute('src', $asset->getFile());
                    } else {
                        $node->setAttribute('href', $asset->getFile());
                    }
                }
            }
        }

        $html = $doc->saveHTML();
        $pageArchive->setHtml($html);

        $newPageArchiveFile = $fileStorageService->addPageArchiveFile($pageArchive);

        //
        // Find URLs inside css files and replace them with according downloaded asset file paths
        // TODO: add support for @import rule without url()
        // TODO: change URLS in inline CSS
        //

        $pageArchiveDir = $fileStorageService->getPageArchiveDir($pageArchive);
        // Itterate through css assets to get content of each css file
        foreach ($assets as $asset) {
            if ($asset->getType() == 'css') {
                $cssFile = $pageArchiveDir.'/'.$asset->getFile();
                $cssContent = file_get_contents($cssFile);
var_dump("CSS FILE: ".$cssFile);
                if ($cssContent) {
                    // Find all url() elements in CSS file and for each of them try to find absolute URL
                    // in assets to replace this relative URLs with absolute ones.
                    $cssContent = preg_replace_callback(
                        '/(?<=url\("|\(\'|\()[^",\'\)]+/',    // match "url(SOME_RELATIVE_OR_ABSOLUTE_URL)"
                        function ($matches) use ($assets) {
                            if (!empty($matches[0])) {
                                $url = $matches[0];
var_dump(" - Found URL in CSS: ".$url);
                                // If URL in CSS is relative, remove ".." so it can be found in absoute URL path
                                if (substr($url, 0, 2) == '..') {
                                    $url = substr_replace($url, '', 0, 2);
var_dump(" - Fixed relative URL: ".$url);
                                }
                                // Now itterate through all assets and try to find absolute URLs for given relative URLs
                                foreach ($assets as $asset) {

                                    // if ($asset->getType() != 'css') {   // We are NOT filtered out css assets here because CSS can contain
                                    // reference to other css files.
                                    // HOWEVER THIS IS DONE THROUGH @import RULE WHICH CAN SPECIFY URL WITHOUT "URL()".
                                    // https://developer.mozilla.org/en/docs/Web/CSS/@import
                                    // e.g. @import 'custom.css';
                                    // !! TODO: add support for @import rule without url() !!

                                    if (strpos($asset->getUrl(), $url) !== false) {
var_dump(" - Matched css URL in asset, returned file: ".$asset->getFile());
                                        return $asset->getFile();   // If URL was found in asset, replace it downloaded asset path
                                    }
                                }
                            }
                            // If URL was not found in any of assets, don't replace it (leave original $matches[0] string)
var_dump(" - No CSS urls matched in assets");
                            return $matches[0];
                        },
                        $cssContent
                    );
                    file_put_contents($cssFile, $cssContent);
                }
            }
        }

        // $processedPageHtml = $diffService->prepareHtmlForDiff($pageArchive->getHtml());
        // $pageArchive->setHtml($processedPageHtml);

        exit("STOP");
    }
}

<?php

namespace Htmldiff\ArchiveBundle\Entity;

use Htmldiff\ArchiveBundle\Entity\Archive;
use Htmldiff\PageBundle\Entity\Page;

class PageArchive
{
    private $archive;

    private $page;

    private $html;

    private $har;

    private $assets;

    // private $cookies;

    public function setArchive(Archive $archive)
    {
        $this->archive = $archive;

        return $this;
    }

    public function getArchive()
    {
        return $this->archive;
    }

    public function setPage(Page $page)
    {
        $this->page = $page;

        return $this;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function setHtml($html)
    {
        $this->html = $html;

        return $this;
    }

    public function getHtml()
    {
        return $this->html;
    }

    public function setHar($har)
    {
        $this->har = $har;

        return $this;
    }

    public function getHar()
    {
        return $this->har;
    }

    public function setAssets($assets)
    {
        $this->assets = $assets;

        return $this;
    }

    public function getAssets()
    {
        return $this->assets;
    }

    // public function setCookies($cookies)
    // {
    //     $this->cookies = $cookies;

    //     return $this;
    // }

    // public function getCookies()
    // {
    //     return $this->cookies;
    // }
}

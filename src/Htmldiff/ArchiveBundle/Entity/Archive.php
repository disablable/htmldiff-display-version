<?php

namespace Htmldiff\ArchiveBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Archive
 *
 * @ORM\Table(name="archive")
 * @ORM\Entity()
 */
class Archive // or ArchiveUpdate or Update ??
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Number of pages to update
     * @ORM\Column(name="page_count", type="integer")
     */
    private $pageCount;

    /**
     * Update started on
     * @ORM\Column(name="started_on", type="datetime")
     */
    private $startedOn;

    /**
     * Update finished on
     * @ORM\Column(name="finished_on", type="datetime", nullable=true)
     */
    private $finishedOn;

    public function getId()
    {
        return $this->id;
    }

    public function setPageCount($pageCount)
    {
        $this->pageCount = $pageCount;

        return $this;
    }

    public function getPageCount()
    {
        return $this->pageCount;
    }

    public function setStartedOn($startedOn)
    {
        $this->startedOn = $startedOn;

        return $this;
    }

    public function getStartedOn()
    {
        return $this->startedOn;
    }

    public function setFinishedOn($finishedOn)
    {
        $this->finishedOn = $finishedOn;

        return $this;
    }

    public function getFinishedOn()
    {
        return $this->finishedOn;
    }
}

<?php

namespace Htmldiff\ArchiveBundle\Entity;

class PageAsset
{
    private $url;

    private $filePath;

    private $mimeType;

    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;

        return $this;
    }

    public function getFilePath()
    {
        return $this->filePath;
    }

    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    public function getMimeType()
    {
        return $this->mimeType;
    }
}

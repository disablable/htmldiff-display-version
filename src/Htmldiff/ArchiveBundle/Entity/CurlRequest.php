<?php

namespace Htmldiff\ArchiveBundle\Entity;

class CurlRequest
{
    private $url;

    private $method;

    private $cookies;

    private $downloadFile;

    private $pid;

    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function setCookies($cookies)
    {
        $this->cookies = $cookies;

        return $this;
    }

    public function getCookies()
    {
        return $this->cookies;
    }

    public function setDownloadFile($downloadFile)
    {
        $this->downloadFile = $downloadFile;

        return $this;
    }

    public function getDownloadFile()
    {
        return $this->downloadFile;
    }

    public function setPid($pid)
    {
        $this->pid = $pid;

        return $this;
    }

    public function getPid()
    {
        return $this->pid;
    }
}

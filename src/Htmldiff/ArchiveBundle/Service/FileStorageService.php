<?php

namespace Htmldiff\ArchiveBundle\Service;

use Monolog\Logger;
use Htmldiff\ArchiveBundle\Entity\PageArchive;
use Htmldiff\PageBundle\Entity\Page;

/**
 * Archive storage file tree along with dir naming:
 *
 *   + storage
 *     + archive/                 <- archive storage dir
 *       + page_id/               <- page archive storage dir
 *         + archive_timestamp/   <- page archive dir
 *           + assets/            <- page archive assets dir
 *             - jpg|css          <- page archive assets
 *             - ...
 *           - index.html         <- page archive file
 *         + ...
 *       + ...
 *     + diff/                    <- generated diff cache storage
 *     + temp/
 */

class FileStorageService
{
    const DIR_STORAGE     = 'storage';
    const DIR_ARCHIVE     = 'archive';
    const DIR_DIFF        = 'diff';
    const DIR_TEMP        = 'temp';
    const FILE_PAGE_INDEX = 'index.html';

    private $logger;
    private $root;

    public function __construct(Logger $logger, $root)
    {
        $this->logger = $logger;
        $this->root = $root;
    }

    public function getLastPageArchiveFile(PageArchive $pageArchive)
    {
        $this->logger->info('Getting last page file from archive');

        $pageId = $pageArchive->getPage()->getId();
        $currentPageArchiveDirName = $pageArchive->getArchive()->getStartedOn()->getTimestamp();
        $pageArchiveStorageDir = $this->getPageArchiveStorageDir($pageId);

        if (false === ($archiveDirs = scandir($pageArchiveStorageDir, SCANDIR_SORT_DESCENDING))) {
            throw new \Exception('Unable to read from page archive dir: '.$pageArchiveDir);
        }

        foreach ($pageArchiveDirs as $pageArchiveDirName) {
            if ($pageArchiveDirName != '.' && $pageArchiveDirName != '..'  && $pageArchiveDirName != $currentPageArchiveDirName) {
                return realpath($pageArchiveStorageDir.'/'.$pageArchiveDirName.'/'.self::FILE_PAGE_INDEX);
            }
        }

        return false;
    }

    public function addPageArchiveFile(PageArchive $pageArchive)
    {
        $this->logger->info('Adding page file to archive');

        $pageArchiveDir = $this->getPageArchiveDir($pageArchive);
        $pageArchiveFilePath = $pageArchiveDir.'/'.self::FILE_PAGE_INDEX;
        $html = $pageArchive->getHtml();

        if (false === file_put_contents($pageArchiveFilePath, $html)) {
            throw new \Exception('Unable to save page file to archive: '.$pageArchiveFilePath);
        }

        return $pageArchiveFilePath;
    }

    public function deletePageArchiveDir($pageArchive)
    {
        $pageArchiveDir = $this->getPageArchiveDir($pageArchive);

        return unlink($pageArchiveDir);
    }

    private function getArchiveStorageDir()
    {
        $path = $this->root.'/../'.self::DIR_STORAGE.'/'.self::DIR_ARCHIVE;
        $archiveDir = $this->ensureDir($path);

        return $archiveDir;
    }

    private function getPageArchiveStorageDir(Page $page)
    {
        $archiveStorageDir = $this->getArchiveStorageDir();
        $pageArchiveStorageDirName = $page->getId();

        $pageArchiveStorageDir = $this->ensureDir($archiveStorageDir.'/'.$pageArchiveStorageDirName);

        return $pageArchiveStorageDir;
    }

    public function getPageArchiveDir(PageArchive $pageArchive)
    {
        $pageArchiveStorageDir = $this->getPageArchiveStorageDir($pageArchive->getPage());
        $archiveDirName = $pageArchive->getArchive()->getStartedOn()->getTimestamp();

        $pageArchiveDir = $this->ensureDir($pageArchiveStorageDir.'/'.$archiveDirName);

        return $pageArchiveDir;
    }

    public function getPageArchiveAssetsDir(PageArchive $pageArchive)
    {
        $pageArchiveDir = $this->getPageArchiveDir($pageArchive);

        $pageArchiveAssetsDir = $this->ensureDir($pageArchiveDir.'/assets');

        return $pageArchiveAssetsDir;
    }

    private function getDiffDir()
    {
        $path = $this->root.'/../'.self::DIR_STORAGE.'/'.self::DIR_DIFF;
        $diffDir = $this->ensureDir($path);

        return $diffDir;
    }

    /**
     * Returns resolved path for given dir path.
     * If dir does not exist, it attempts to create it.
     */
    private function ensureDir($dirPath)
    {
        if (strlen($dirPath) == 0) {
            throw new \Exception('Unable to ensure empty path as dir');
        }

        if (false === $dir = realpath($dirPath)) {
            if (false === mkdir($dirPath, 0775)) {
                throw new \Exception('Unable to create dir: "'.$dirPath);
            }

            if (false === $dir = realpath($dirPath)) {
                throw new \Exception('Unable to ensure dir: "'.$dirPath);
            }
        }

        return $dir;
    }



















    public function saveDiffToArchive($diffHtml, $file1, $file2)
    {
        $this->logger->info('Saving diff to archive');

        $diffPageFilePath = $this->createDiffFilePath($file1, $file2);

        $this->logger->info(' -> ' . $diffPageFilePath);

        if (false === file_put_contents($diffPageFilePath, $diffHtml)) {
            throw new \Exception('Unable to save page diff file to: ' . $diffPageFile);
        }

        return $diffPageFilePath;
    }

    // public function savePageAssetsToArchive(Archive $archive, Page $page, $assets = [])
    // {
    //     $this->logger->info('Saving page assets to archive');

    //     $pageArchiveDir = $this->getPageArchiveStorageDir($page) . '/' . $archive->getStartedOn()->getTimestamp();

    //     foreach ($assets as $asset) {
    //         // Pitanje je da li je ovo ok zbog sessiona. Trebalo bi preko seleniuma
    //         $command = 'wget -v --convert-links --page-requisites --directory-prefix=' . escapeshellarg($pageArchiveDir) . ' ' . escapeshellarg($page->getUrl());
    //         // Catch errors and only log them (don't throw exception)
    //         exec($command, $diffArray, $output);
    //     }

    //     echo($command);exit("STOP");
    // }



    public function readFile($file)
    {
        $this->logger->info('Reading from file: ' . $file);

        if (file_exists($file)) {
            return file_get_contents($file);
        }

        return false;
    }

    public function deleteFile($file)
    {
        $this->logger->info('Deleting file: ' . $file);

        if (file_exists($file)) {
            return unlink($file);
        }

        return false;
    }

    public function createDiffFilePath($filePath1, $filePath2)
    {
        $diffDir = $this->getDiffDir();
        $fileName1 = $this->getFileNameFromPath($filePath1);
        $fileName2 = $this->getFileNameFromPath($filePath2);

        return $diffDir . '/' . $fileName1 . '-' . $fileName2 . '.html';
    }

    private function getFileNameFromPath($path)
    {
        $pathInfo = pathinfo($path);

        return $pathInfo['basename'];
    }
}

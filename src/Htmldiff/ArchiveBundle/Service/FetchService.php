<?php

namespace Htmldiff\ArchiveBundle\Service;

use Doctrine\ORM\EntityManager;
use Htmldiff\ArchiveBundle\Entity\CurlRequest;
use Htmldiff\ArchiveBundle\Entity\PageArchive;
use Htmldiff\ArchiveBundle\Entity\PageAsset;
use Htmldiff\ArchiveBundle\Service\CurlService;
use Htmldiff\ArchiveBundle\Service\FileStorageService;
use Monolog\Logger;

/**
 * Class responsible for fetching page source for given URL.
 */
class FetchService // SelenuimService ??
{
    private $logger;
    private $em;
    private $curl;
    private $webDriver;
    private $proxy;
    private $fileStorageService;

    public function __construct(Logger $logger, EntityManager $em, CurlService $curl, FileStorageService $fileStorageService, $seleniumHostHub)
    {
        $this->logger = $logger;
        $this->em = $em;
        $this->curl = $curl;
        $this->fileStorageService = $fileStorageService;
        $this->webDriver = $this->getWebDriver($seleniumHostHub);
    }

    public function __destruct()
    {
        $this->proxy->close();
        $this->webDriver->manage()->deleteAllCookies();
        $this->webDriver->quit();
    }

    public function fetchHtml(PageArchive $pageArchive)
    {
        $this->logger->info('Fetching page HTML..');

        $page = $pageArchive->getPage();

        // Open new HAR session with page id as unique name
        $this->proxy->newHar($page->getId());

        // Navigate to url
        // TODO: Add handling of selenium response codes (404, ...).
        $this->logger->info(' - Navigating to: ' . $page->getUrl());

        $this->webDriver->get($page->getUrl());

        // $this->logger->info('Applying JS code');

        // $applyJS = $page->getApplyJSCode();

        // TODO: Ovdje treba iscupati page base url, jer ako je korisnik zadao www.google.com/a/b/c
        // base url je www.google.com !!
        // ovo mogu sa PHP-om: parse_url()

        // $this->webDriver->executeScript(
        //     'var baseEl = document.getElementsByTagName("base");
        //     if (baseEl.length == 0) {
        //         var headEl = document.getElementsByTagName("head")[0];
        //         baseEl = document.createElement("base");
        //         baseEl.setAttribute("href", "'.$page->getUrl().'");
        //         var firstChild = headEl.firstChild;
        //         headEl.insertBefore(baseEl, firstChild);
        //     }'
        // );

        // Get rendered page source
        $this->logger->info(' - Rendering page source');

        $pageArchive->setHtml($this->webDriver->getPageSource());

        $cookies = $this->webDriver->manage()->getCookies();
//file_put_contents('/var/www/htmldiff/storage/archive/782/cookies.txt', print_r($cookies, 1));

//file_put_contents('/var/www/htmldiff/storage/archive/782/har.txt', print_r($this->proxy->har, 1));

        // Get all css and image requests form HAR
        $pageArchive->setHar($this->filterAssetsFromHar($this->proxy->har));

//file_put_contents('/var/www/htmldiff/storage/archive/782/filtered_har.txt', print_r($pageArchive->getHar(), 1));

        //$this->proxy->close();
        $this->webDriver->manage()->deleteAllCookies();
        $this->webDriver->close();
    }

    /*public function downloadAssets(PageArchive $pageArchive)
    {
        $page = $pageArchive->getPage();
        $pageHtml = $pageArchive->getHtml();
        $pageUrl = $page->getUrl();
        $pageUrlParts = self::getUrlVariationsFromAbsoluteUrl($pageUrl);

        $cssAssets = [];

        //
        // First find and download all CSS files
        //
        // http://www.w3.org/TR/html401/present/styles.html
        // CSS style can be provided as:
        //  - 14.2.2 Inline style information (<p style="font-size: 12pt;" />)
        //  - 14.2.3 Header style information: the STYLE element (<style type="text/css">h1 {border-width:1; border:solid; text-align:center}</style>)
        //  - 14.3 External style sheets (<link href="http://www.acme.com/corporate.css" />)
        //  - 14.6 Linking to style sheets with HTTP headers  (Link: <http://www.acme.com/corporate.css>; REL=stylesheet)
        //
        //  Also CSS can be injected like this:
        // ??? <meta name="dynamic-stylesheet" id="editor-css-url" content="editor-6fccdab8fd62df326b091f81776a8ede.css"> ???
        //

        $doc = new \DOMDocument();
        $doc->loadHTML($pageHtml);
        $xPath = new \DOMXPath($doc);

        $curlRequests = [];
        $cssNodes = $xPath->query('//link[@href] | //meta[@content]');
        foreach ($cssNodes as $cssNode) {
var_dump($cssNode->getAttribute('type'));
            // Filter link element by checking attribute "rel". It should contain word "stylesheet".
            if (strpos($cssNode->getAttribute('rel'), 'stylesheet') === false) {
                continue;
            }
            // if (strpos($cssNode->getAttribute('rel'), 'stylesheet') === false) {

            // }

            $href = $cssNode->getAttribute('href');

            //
            // If this is relative URL, create absolute
            //

            // This will capture relative URLs starting with "/" or "../" and not followed by another "/" char
            // so this "//some/schemaless/style.css" will NOT be captured
            if (preg_match('/^(\/|..\/)[^\/]+/', $href)) {
                // If URL starts with "../", prepend "/"
                if (substr($href, 0, 2) == '..') {
                    $href = '/'.$href;
                }
                // Prepend scheme and authority to relative URL
                $href = $pageUrlParts['scheme'].'://'.$pageUrlParts['authority'].$href;
            }

            $downloadPath = $this->fileStorageService->getPageArchiveAssetsDir($pageArchive);
            $fileName = md5($href).'.css';
            $downloadFile = $downloadPath.'/'.$fileName;
            $assetFile = 'assets/'.$fileName;

            // Create CurlRequest
            $curlRequest = new CurlRequest();
            $curlRequest->setUrl($href);
            $curlRequest->setMethod('GET');
            //$curlRequest->setCookies([]);
            $curlRequest->setDownloadFile($downloadFile);
            $curlRequests[] = $curlRequest;

            // Create PageAsset
            $asset = new PageAsset();
            $asset->setType('css');
            $asset->setUrl($href);
            $asset->setFile($assetFile);
            $cssAssets[] = $asset;

            // Replace CSS href in CSS node with downloaded file path
            $cssNode->setAttribute('href', $assetFile);
        }

        $html = $doc->saveHTML();
        $pageArchive->setHtml($html);

        // Download CSS assets
        $this->curl->multiDownload($curlRequests);

        //
        // CSS files downloaded
    }*/









    // Using browsermob proxy
    public function downloadAssets(PageArchive $pageArchive)
    {
        $pageArchiveDir = $this->fileStorageService->getPageArchiveDir($pageArchive);
        $pageArchiveAssetsDir = $this->fileStorageService->getPageArchiveAssetsDir($pageArchive);
        $har = $pageArchive->getHar();
        $curlRequests = [];
        $assets = [];

        //
        // Find elements with src attributes in HAR array and replace their paths with according downloaded asset file paths
        // Find link elements in HAR array and replace their paths with according downloaded asset file paths
        //
        // TODO: When fixing relative URLs handle multiple ".." (e.g. "../../../some.css")
        //   - Thought: if we replace "../" with "/" as we do, we will get "/../../some.css" which still we will not be able to
        //     match in HARs absolute URL (e.g. "http://www.page.com/some/path/to/some.css").
        //     But if we strip all ../../../ chars, then we will be left with "some.css" which can be matched in absolute
        //     URL above.
        //
        //
        $doc = new \DOMDocument();
        $doc->loadHTML($pageArchive->getHtml());
        $xPath = new \DOMXPath($doc);

        $nodes = $xPath->query('//*[@src] | //link[@href]');
        foreach ($nodes as $node) {
            // Resolve if this node has "src" or "href" attribute
            // and get URL from it.
            if (!empty($node->getAttribute('src'))) {
                $url = $node->getAttribute('src');
                $nodeType = 'src';
            } else {
                $url = $node->getAttribute('href');
                $nodeType = 'href';
            }

            // Fix relative URL (This still needs to be improved due to comment on L:206)
            // TODO: make this in separate method
            if (substr($url, 0, 3) == '../') {
                $url = substr_replace($url, '/', 0, 3);
            } elseif (substr($url, 0, 2) == './') {
                $url = substr_replace($url, '/', 0, 2);
            }

            // Find if this node URL has been captured by proxy in HAR.
            // If it is, then this is required asset so replace its URL with
            // path to downloaded asset file (we will need to download it later).
            foreach ($har as $entry) {
                $harUrl = $entry['request']['url'];
                $harMimeType = $entry['response']['content']['mimeType'];
                $harMethod = $entry['request']['method'];
                $harCookies = $entry['request']['cookies'];

                if (strpos($harUrl, $url) !== false) {

                    // TODO: $assetFileExtension = $this->guessFileExtension($url, $mimeType);
                    // This will fist look if there is extension in URL (e.g. "site.com/assets/style.css"), and if not
                    // then it will try to get extension based on mime type.
                    $assetFileName = $fileName = md5($harUrl);
                    $assetDownloadFilePath = $pageArchiveAssetsDir.'/'.$assetFileName;

                    // Create PageAsset
                    $asset = new PageAsset();
                    $asset->setUrl($harUrl);
                    $asset->setFilePath('assets/'.$assetFileName);
                    $asset->setMimeType($harMimeType);
                    $assets[] = $asset;

                    // Create CurlRequest (for later download)
                    $curlRequest = new CurlRequest();
                    $curlRequest->setUrl($harUrl);
                    $curlRequest->setMethod(!empty($harMethod) ? $harMethod : 'GET');
                    $curlRequest->setCookies(!empty($harCookies) ? $harCookies : []);
                    $curlRequest->setDownloadFile($assetDownloadFilePath);
                    $curlRequests[] = $curlRequest;

                    // Replace URL of asset with downloaded file path
                    if ($nodeType == 'src') {
                        $node->setAttribute('src', $asset->getFilePath());
                    } else {
                        $node->setAttribute('href', $asset->getFilePath());
                    }
                }
            }
        }

        // Update modified HTML in PageArchive
        $html = $doc->saveHTML();
        $pageArchive->setHtml($html);

        // Download assets for HTML
        $this->curl->multiDownload($curlRequests);

        // Empty downloaded curl requests
        $curlRequests = [];

        //
        // Find URLs inside css files and replace them with according downloaded asset file paths
        // TODO: add support for @import rule without url()
        // TODO: change URLS in inline CSS
        // TODO: add support for fonts injected inside CSS
        //

        // Itterate through css assets to get content of each css file
        foreach ($assets as $asset) {
            if (strpos($asset->getMimeType(), 'css') !== false) {
                $cssFile = $pageArchiveDir.'/'.$asset->getFilePath();
                $cssContent = file_get_contents($cssFile);
                if ($cssContent) {
var_dump("CSS FILE: ".$cssFile);
                    // Find all url() elements in CSS file and for each of them try to find absolute URL
                    // in HAR to replace this relative URLs with absolute ones.
                    $cssContent = preg_replace_callback(
                        '/(?<=url\()[^\)]+(?=\))+/',    // match URL wrapped inside "url()" string but without that wrapper
                        function ($matches) use ($pageArchiveAssetsDir, $har, &$curlRequests) {
                            if (!empty($matches[0])) {
                                // Remove quotes and double quotes from URL
                                $url = str_replace(['"', '\''], '', $matches[0]);

                                // Fix relative URL (This still needs to be improved due to comment on L:206)
                                // TODO: make this in separate method
                                if (substr($url, 0, 3) == '../') {
                                    $url = substr_replace($url, '/', 0, 3);
                                } elseif (substr($url, 0, 2) == './') {
                                    $url = substr_replace($url, '/', 0, 2);
                                }
var_dump("URL: ".$url);
                                // Itterate through HAR requests and try to find absolute URLs for given relative URL from CSS content
                                foreach ($har as $entry) {
                                    $harUrl = $entry['request']['url'];
                                    $harMimeType = $entry['response']['content']['mimeType'];
                                    $harMethod = $entry['request']['method'];
                                    $harCookies = $entry['request']['cookies'];

                                    // if ($asset->getType() != 'css') {   // We are NOT filtered out css assets here because CSS can contain
                                    // reference to other css files.
                                    // HOWEVER THIS IS DONE THROUGH @import RULE WHICH CAN SPECIFY URL WITHOUT "URL()".
                                    // https://developer.mozilla.org/en/docs/Web/CSS/@import
                                    // e.g. @import 'custom.css';
                                    // !! TODO: add support for @import rule without url() !!

                                    if (strpos($harUrl, $url) !== false) {
                                        $assetFileName = $fileName = md5($harUrl);
                                        $assetDownloadFilePath = $pageArchiveAssetsDir.'/'.$assetFileName;

                                        // Create CurlRequest (for later download)
                                        $curlRequest = new CurlRequest();
                                        $curlRequest->setUrl($harUrl);
                                        $curlRequest->setMethod(!empty($harMethod) ? $harMethod : 'GET');
                                        $curlRequest->setCookies(!empty($harCookies) ? $harCookies : []);
                                        $curlRequest->setDownloadFile($assetDownloadFilePath);
                                        $curlRequests[] = $curlRequest;
var_dump("CNT: :".count($curlRequests));
                                        // If URL was found in asset, replace it downloaded asset file.
                                        // Path is not needed here because CSS file is already in "assets" folder.
                                        return $assetFileName;
                                    }
                                }
                            }
                            // If URL was not found in any of assets, don't replace it (leave original $matches[0] string)
                            return $matches[0];
                        },
                        $cssContent
                    );
                    file_put_contents($cssFile, $cssContent);
                }
            }
        }

print_r($curlRequests);
        // Download assets for CSS files
        $this->curl->multiDownload($curlRequests);




/*
        // Prepare CurlRequests and Assets from HAR entries
        foreach ($har as $key => $entry) {
            $downloadPath = $this->fileStorageService->getPageArchiveAssetsDir($pageArchive);
            $mimeType = $entry['response']['content']['mimeType'];
            $fileExtension = self::getExtensionForMimeType($mimeType);
            $fileName = md5($entry['request']['url']).'.'.$fileExtension;

            $downloadFile = $downloadPath.'/'.$fileName;
            $assetFile = 'assets/'.$fileName;

            // Create CurlRequest
            $curlRequest = new CurlRequest();
            $curlRequest->setUrl($entry['request']['url']);
            $curlRequest->setMethod(!empty($entry['request']['method']) ? $entry['request']['method'] : 'GET');
            $curlRequest->setCookies(!empty($entry['request']['cookies']) ? $entry['request']['cookies'] : []);
            $curlRequest->setDownloadFile($downloadFile);

            $curlRequests[] = $curlRequest;

            // Create PageAsset
            $asset = new PageAsset();
            $asset->setType($fileExtension);
            $asset->setUrl($entry['request']['url']);
            $asset->setFile($assetFile);

            $assets[] = $asset;
        }

        // Download assets
        $this->curl->multiDownload($curlRequests);

        // Set assets to PageArchive
        $pageArchive->setAssets($assets);

        return;



        //
        // Replace assets URLs with path to downloaded asset files in HTML FILE and CSS FILES
        //


        // URLs can be:
        //  - absolute: http://cdn.somesite.com/abc/image.png?ver=2#top
        //  - relative: /abc/image.png
        //  - without scheme: //cdn.somesite.com/abc/image.png?ver=2#top
        //  - ???

        $html = $pageArchive->getHtml();
        $pageArchiveDir = $this->fileStorageService->getPageArchiveDir($pageArchive);
        foreach ($assets as $asset) {

            // Use htmlspecialchars on urls because html contains encoded htmlspecial chars (?a=1&b=2 is ?a=1&amp;b=2)

            $url = $asset->getUrl();
            $urlVariations = self::getUrlVariationsFromAbsoluteUrl($url);
            $file = $asset->getFile();

            // First replace absolute URLs
            $html = str_replace(htmlspecialchars($url), $file, $html, $cnt);
            $this->logger->info('HTML - replacing '.htmlspecialchars($url).': '.$cnt);

            // Then replace schemeless URLs
            $html = str_replace(htmlspecialchars($urlVariations['schemeless']), $file, $html);
            $this->logger->info('HTML - replacing '.htmlspecialchars($urlVariations['schemeless']).': '.$cnt);

            // Then replace relative URLs
            $html = str_replace(htmlspecialchars($urlVariations['relative']), $file, $html);
            $this->logger->info('HTML - replacing '.htmlspecialchars($urlVariations['relative']).': '.$cnt);

            $this->logger->info('with '.$file);

            // Replace assets URLs inside CSS files
            if ($asset->getType() == 'css') {
                $cssFile = $pageArchiveDir.'/'.$asset->getFile();
                $cssFileContent = file_get_contents($cssFile);
                // Itterate through all fetched assets and try to replace every occurence
                // of them in current CSS asset
                foreach ($assets as $asset2) {
                    $url = $asset2->getUrl();
                    $urlVariations = self::getUrlVariationsFromAbsoluteUrl($url);
                    $file = $asset2->getFile();
                    // First replace absolute URLs
                    $cssFileContent = str_replace(htmlspecialchars($url), $file, $cssFileContent, $cnt);
                    $this->logger->info('CSS - replacing '.htmlspecialchars($url).': '.$cnt);

                    // Then replace schemeless URLs
                    $cssFileContent = str_replace(htmlspecialchars($urlVariations['schemeless']), $file, $cssFileContent);
                    $this->logger->info('CSS - replacing '.htmlspecialchars($urlVariations['schemeless']).': '.$cnt);

                    // Then replace relative URL
                    $cssFileContent = str_replace(htmlspecialchars($urlVariations['relative']), $file, $cssFileContent);
                    $this->logger->info('CSS - Replacing '.htmlspecialchars($urlVariations['relative']).': '.$cnt);

                    $this->logger->info('with '.$file);
                }
                file_put_contents($cssFile, $cssFileContent);
            }
        }
        $pageArchive->setHtml($html);
*/
    }

    private function getWebDriver($seleniumHostHub)
    {
        $this->logger->info('Connecting to selenium hub: ' . $seleniumHostHub);

        $proxyIp = '172.17.0.4';
        $this->proxy = new \PHPBrowserMobProxy_Client($proxyIp.':9090');
        $this->proxy->open();

        // Mozda da ovdje probam jedan retry ako $this->proxy->port nije definiran
        // todo..

        // Connect to Selenium hub
        // $desiredCapabilities = [
        //     \DesiredCapabilities::chrome(),
        // ];

        $capabilities = [
            \WebDriverCapabilityType::BROWSER_NAME => 'firefox',
            \WebDriverCapabilityType::PROXY => [
                'proxyType' => 'manual',
                'httpProxy' => $proxyIp.':'.$this->proxy->port,
                'sslProxy' => $proxyIp.':'.$this->proxy->port,
            ],
        ];

        $this->webDriver = \RemoteWebDriver::create($seleniumHostHub, $capabilities, 5000);

        return $this->webDriver;
    }

    private function filterAssetsFromHar($har)
    {
        $filteredHar = [];

        // Filter HAR requests:
        //  - exclude content with no mime type detected
        //  - exclude javasctipts
        //  - exclude empty content (0 bytes size)
        //  - exclude content bigger then 1 MBoctet-stream
        //
        //  We can not create whitelist because e.g. we can get font ".woff2" with "binary/octet-stream" mime type
        //
        if (!empty($har['log']['entries'])) {
            // Cycle through HAR 'entries' array..
            foreach ($har['log']['entries'] as $entry) {
                // Look for the 'mimeType' key
                if (isset($entry['request']['url']) &&
                    isset($entry['response']['content']['mimeType'])
                ) {
                    $mimeType = $entry['response']['content']['mimeType'];
                    // Exclude JS, HTML, JSON
                    if (strpos($mimeType, 'script') > 0 ||
                        strpos($mimeType, 'html') > 0 ||
                        strpos($mimeType, 'json') > 0
                    ) {
                        continue;
                    }
                    // Check content size
                    if (isset($entry['response']['content']['size'])) {
                        $size = $entry['response']['content']['size'];
                        if ($size == 0 || $size > (1 * 1024 * 1024)) {
                            continue;
                        }
                    }
                    $filteredHar[] = $entry;
                }
            }
        }

        return $filteredHar;
    }

    private static function getExtensionForMimeType($mimeType)
    {
        $extensions = self::getExtensions();

        foreach ($extensions as $ext => $mimeTypeGroup) {
            if (in_array($mimeType, $mimeTypeGroup)) {
                return $ext;
            }
        }

        // If exact mime type is not found, try to guess..
        if (strpos($mimeType, 'font')) {
            return 'font';
        } //else if ()_{}

        return false;
    }

    private static function getExtensions()
    {
        return [
            'css'   => ['text/css', 'text/plain'],
            'jpg'   => ['image/jpeg', 'image/pjpeg'],
            'png'   => ['image/png',  'image/x-png'],
            'tiff'  => ['image/tiff'],
            'gif'   => ['image/gif'],
            'bmp'   => ['image/bmp', 'image/x-bmp', 'image/x-bitmap', 'image/x-xbitmap', 'image/x-win-bitmap', 'image/x-windows-bmp', 'image/ms-bmp', 'image/x-ms-bmp', 'application/bmp', 'application/x-bmp', 'application/x-win-bitmap'],
            'ico'   => ['image/x-icon', 'image/x-ico', 'image/vnd.microsoft.icon'],
            'ttf'   => ['application/x-font-ttf'],
            'woff'  => ['application/x-font-woff'],
            'gsf'   => ['application/x-font-ghostscript'],
            'bdf'   => ['application/x-font-bdf'],
            'eot'   => ['application/vnd.ms-fontobject'],
            'otf'   => ['application/x-font-otf'],
            'pcf'   => ['application/x-font-pcf'],
            'pfr'   => ['application/font-tdpfr'],
            // TODO: add other font types from: http://www.freeformatter.com/mime-types-list.html
            // Also add other file types that could be assets on web pages....
        ];
    }

    private static function getUrlVariationsFromAbsoluteUrl($absUrl)
    {
        //
        // http://tools.ietf.org/html/rfc3986#section-3
        //
        //    foo://user:pass@example.com:8042/over/there?name=ferret#nose
        //    \_/   \________________________/\_________/ \_________/ \__/
        //     |                 |                 |           |        |
        //  scheme           authority           path        query   fragment
        //

        $urlParts = parse_url($absUrl);

        $scheme = !empty($pageUrlParts['scheme']) ? $pageUrlParts['scheme'] : '';

        // Authority
        $authority = '';
        if (!empty($urlParts['user'])) {
            $authority .= $urlParts['user'];
            if (!empty($urlParts['pass'])) {
                $authority .= ':'.$urlParts['pass'];
            }
            $authority .= '@';
        }
        $authority .= $urlParts['host'];
        if (!empty($urlParts['port'])) {
            $authority .= ':'.$urlParts['port'];
        }

        // Build relative URL
        $relativeUrl = '';
        if (!empty($urlParts['path'])) {
            $relativeUrl = $urlParts['path'];
            if (!empty($urlParts['query'])) {
                $relativeUrl .= '?'.$urlParts['query'];
            }
            if (!empty($urlParts['fragment'])) {
                $relativeUrl .= '#'.$urlParts['fragment'];
            }
        }

        $schemalessUrl = $authority.$relativeUrl;

        return [
            'scheme'     => $scheme,
            'authority'  => $authority,
            'relative'   => $relativeUrl,
            'schemeless' => $schemalessUrl
        ];
    }
}




// Ovo je ok, ali samo za img elemente
// Bolje rjesenje je proxy koji intercept-a promet i sprema u fajlove
/*$imageData = $this->webDriver->executeScript('
    // Save rendered img data as base64 encoded string without requesting them again from server
    var img=document.getElementsByTagName("img")[1];
    var canvas=document.createElement("canvas");
    canvas.width=img.width;
    canvas.height=img.height;
    var ctx=canvas.getContext("2d");
    ctx.drawImage(img,0,0);
    var dataURL=canvas.toDataURL("image/png");

    return dataURL;
');


// Dakle imam HAR od requesta i sad mogu filtrirati image i css fajlove i spremati ih na disk pojedinacnim pozivanjem
// tih resource-a preko seleniuma (dakle gdje imam session).
var_dump($imageData);
file_put_contents('/var/www/htmldiff/storage/temp/HAR.json', json_encode($this->proxy->har));
*/

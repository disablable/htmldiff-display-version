<?php

namespace Htmldiff\ArchiveBundle\Service;

use Doctrine\ORM\EntityManager;
use Htmldiff\ArchiveBundle\Entity\PageArchive;
use Htmldiff\ArchiveBundle\Service\DiffService;
use Htmldiff\ArchiveBundle\Service\FetchService;
use Htmldiff\ArchiveBundle\Service\FileStorageService;
use Monolog\Logger;

class PageArchiveService
{
    private $logger;
    private $em;
    private $fetchService;
    private $fileStorageService;
    private $diffService;

    public function __construct(
        Logger $logger,
        EntityManager $em,
        FetchService $fetchService,
        FileStorageService $fileStorageService,
        DiffService $diffService
    ) {
        $this->logger = $logger;
        $this->em = $em;
        $this->fetchService = $fetchService;
        $this->fileStorageService = $fileStorageService;
        $this->diffService = $diffService;
    }

    public function createPageArchive($archiveId, $pageId)
    {
        $this->logger->info("Creating archive for page id: {$pageId}");

        // Load Archive and Page entities
        $archiveRepo = $this->em->getRepository('HtmldiffArchiveBundle:Archive');
        $pageRepo = $this->em->getRepository('HtmldiffPageBundle:Page');

        $archive = $archiveRepo->findOneById($archiveId);
        $page = $pageRepo->findOneById($pageId);

        // Create new PageUpdate entity
        $pageArchive = new PageArchive();
        $pageArchive->setArchive($archive);
        $pageArchive->setPage($page);

        // Fetch page HTML
        $this->fetchService->fetchHtml($pageArchive);

        // Download page assets
        $this->fetchService->downloadAssets($pageArchive);

        // Prepare fetched page source html for diff process
        $processedPageHtml = $this->diffService->prepareHtmlForDiff($pageArchive->getHtml());
        $pageArchive->setHtml($processedPageHtml);

        // Get last page archive file
        $lastPageArchiveFile = $this->fileStorageService->getLastPageArchiveFile($pageArchive);

        // Store processed html and it's assets. This storage is used as version storage.
        $newPageArchiveFile = $this->fileStorageService->addPageArchiveFile($pageArchive);

        // Generate diff against last page archive
        $diffHtml = $this->diffService->generateDiff($lastPageArchiveFile, $newPageArchiveFile);

        if ($diffHtml === null) {
            // Version is no different then last so delete newly created page archive dir
            $this->fileStorageService->deletePageArchiveDir($pageArchive);
        } else {
            // New version is different..

            // Save diff to cache storage (since it is already generated)
            $this->fileStorageService->saveDiffToArchive($diffHtml, $lastPageArchiveFile, $newPageArchiveFile);

            // TODO: Notify user via email ...
        }
    }
}

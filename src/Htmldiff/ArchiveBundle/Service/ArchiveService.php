<?php

namespace Htmldiff\ArchiveBundle\Service;

use Monolog\Logger;
use Doctrine\ORM\EntityManager;

/* Tu prebaciti logiku iz ArchiveUpdateCommand i maknuti loadArchiveById() metodu */

class ArchiveService
{
    private $logger;
    private $em;

    public function __construct(Logger $logger, EntityManager $em)
    {
        $this->logger = $logger;
        $this->em = $em;
    }

    public function loadArchiveById($archiveId)
    {
        $archiveRepo = $this->em->getRepository('HtmldiffArchiveBundle:Archive');
        $archive = $archiveRepo->findOneById($archiveId);

        return $archive;
    }
}

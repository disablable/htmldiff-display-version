<?php

namespace Htmldiff\ArchiveBundle\Service;

use Htmldiff\ArchiveBundle\Entity\CurlRequest;
use Monolog\Logger;

class CurlService
{
    private $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function multiDownload($curlRequests)
    {
        $this->logger->info('Downloading '.count($curlRequests).' files');

        $maxRunning = 100;
        $totalDownloads = count($curlRequests);
        $running = [];

$dbg = [];

        while (count($curlRequests) > 0) {
            if (count($running) < $maxRunning) {
                $curlRequest = array_shift($curlRequests);
$dbg[] = [
    'url' => $curlRequest->getUrl(),
    'file' => $curlRequest->getDownloadFile()
];
                $this->curl($curlRequest);
                $running[] = $curlRequest->getPid();
            } else {
                usleep(500000);
                foreach ($running as $key => $cmd) {
                    if (!$this->isRunning($cmd)) {
                        unset($running[$key]);
                    }
                }
            }
        }

        while (count($running) > 0) {
            foreach ($running as $key => $cmd) {
                if (!$this->isRunning($cmd)) {
                    unset($running[$key]);
                }
            }
            usleep(500000);
        }
file_put_contents('/var/www/htmldiff/storage/archive/780/download.txt', print_r($dbg, 1));
    }

    private function curl($curlRequest)
    {
        // Build curl command
        $cmd = $this->buildCurlCmd($curlRequest->getUrl(), $curlRequest->getMethod(), $curlRequest->getCookies(), $curlRequest->getDownloadFile());
//$this->logger->info($cmd);
        // System process id of running curl command
        $pid = [];

        // Execute command
        // exec(sprintf("%s > %s 2>&1 & echo $!", $cmd, $output), $pid);
        $exec = exec($cmd.' > /dev/null 2>&1 & echo $!', $pid);
        $pid = array_shift($pid);

        $curlRequest->setPid($pid);
    }

    private function buildCurlCmd($url, $method, $cookies, $downloadFile)
    {
        // Example: $cmd = 'curl -o storage/archive/assets/ubuntu.iso "http://releases.ubuntu.com/14.04.3/ubuntu-14.04.3-desktop-amd64.iso"';
        $cookiesString = false;
        $cookiesArr = [];
        if (!empty($cookies)) {
            foreach ($cookies as $cookie) {
                $cookiesArr[] = sprintf('%s=%s', $cookie['name'], $cookie['value']);
            }
            $cookiesString = '-b '.escapeshellarg(implode(';', $cookiesArr));
        }

        //$cmd = 'curl -A Firefox '.escapeshellarg($cookiesString).' -o '.escapeshellarg($downloadFile).' '.escapeshellarg($url);
        if (false === $cookiesString) {
            $cmd = 'curl --compressed '.$cookiesString.' -o '.escapeshellarg($downloadFile).' '.escapeshellarg($url);
        } else {
            $cmd = 'curl --compressed -o '.escapeshellarg($downloadFile).' '.escapeshellarg($url);
        }

        return $cmd;
    }

    private function isRunning($pid)
    {
        try {
            $result = shell_exec(sprintf("ps %d", $pid));
            if (count(preg_split("/\n/", $result)) > 2) {
                return true;
            }
        } catch (Exception $e) {
        }

        return false;
    }
}
